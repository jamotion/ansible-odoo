![License MIT](https://img.shields.io/badge/license-MIT-blue.svg) [![](https://img.shields.io/docker/stars/jamotion/ansible-odoo.svg)](https://hub.docker.com/r/jamotion/ansible-odoo 'DockerHub') [![](https://img.shields.io/docker/pulls/jamotion/ansible-odoo.svg)](https://hub.docker.com/r/jamotion/ansible-odoo 'DockerHub')

Docker image for odoo deployment based on ansible scripts.

